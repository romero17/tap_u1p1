
package mx.tecnm.morelia.practica1;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import sun.print.ServiceDialog;

/**
 *
 * @author Andrea
 */
public class Ventana extends JFrame{
    
 Ventana() {
        super("Mi Ventana");
        
        setSize(400, 300);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        
        Container c = this.getContentPane();
        c.setLayout(new FlowLayout());
        
        JLabel label = new JLabel("Introduce tu nombre:");
        c.add(label);
        ;
        
        JTextField field = new JTextField(15);
        c.add(field);
        
        JButton boton = new JButton("Presioname");
        boton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null,"Hola " + field.getText());
            }
        });
        c.add(boton);
        
        JButton boton2 = new JButton("V2");
        boton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Ventana2(field.getText()).setVisible(true);
            }
        });
        c.add(boton2);
    }
    
    public static void main(String[] args) {
        System.out.println("Mi primer ventana");
        new Ventana().setVisible(true);
    }

}
